package com.example.currencydemo

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.currencydemo.core.presentation.CurrencyActivity
import com.example.currencydemo.util.EspressoIdlingResource
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CurrencyActivityEspressoTest {

    @get:Rule
    private val activityRule = ActivityTestRule(CurrencyActivity::class.java, false, false)


    /**
     * Test scenario states:
     * When app is opened, it shows shimmer effect while loading the data from API in the background.
     * Check the visibility of shimmer view, recycler view and no content view.
     * Shimmer view should be VISIBLE
     * Recycler view should be GONE
     * No content view should be GONE
     */
    @Test
    fun testShimmerViewIsVisible() {

        activityRule.launchActivity(null)

        onView(withId(R.id.progress_bar))
            .check(matches(isCompletelyDisplayed()))

        onView(withId(R.id.items_recyclerview)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_content_textView)).check(matches(not(isDisplayed())))
    }


    /**
     * Test scenario states:
     * After API call is successful, use IdleResource to check views visibility
     * EspressoIdlingResources take care of that in {@link SimpleCountingIdlingResource}
     */
    @Test
    fun testViewsAfterAPISuccess() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
        activityRule.launchActivity(null)

        onView(withId(R.id.items_recyclerview)).check(matches((isDisplayed())))
        onView(withId(R.id.progress_bar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_content_textView)).check(matches(not(isDisplayed())))
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }


}