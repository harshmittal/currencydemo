package com.example.currencydemo.core.presentation

import android.os.Build
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1], manifest = Config.NONE)
class CurrencyActivityTest {

    private var activity: CurrencyActivity? = null

    @Before
    fun setup() {
        activity =
            Robolectric.buildActivity<CurrencyActivity>(CurrencyActivity::class.java).create().resume()
                .get()
    }


    /**
     * Test scenario states:
     * Test for default currency value. It should not be null or empty
     */
    @Test
    fun testForCurrencyValue(){
        Assert.assertTrue(activity?.mSelectedCurrencyValue!!.isNotEmpty())
    }
}