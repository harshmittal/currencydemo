package com.example.currencydemo.core.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencydemo.core.*
import com.example.currencydemo.core.domain.CurrencyUseCases
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class CurrencyViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    val useCases = mock<CurrencyUseCases>()

    lateinit var viewModel: CurrencyViewModel


    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun initTest() {

        viewModel = CurrencyViewModel(
            useCases,
            coroutinesTestRule.testDispatcher,
            coroutinesTestRule.testDispatcher
        )
    }


    @Test
    fun testLoadedState() {

        coroutinesTestRule.testDispatcher.runBlockingTest {

            val currencyData = currencyMockData()
            whenever(useCases.getCurrency("EUR")).thenReturn(currencyData)

            val liveData = viewModel.listState

            viewModel.getCurrencyList("EUR")

            liveData.observeForTesting {
                assert(liveData.value is LoadedState)
            }
        }
    }

}