package com.example.currencydemo.core.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencydemo.core.mock
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class CurrencyUsesCasesTest  {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    val useCases =
        mock<CurrencyUseCases>()


    /**
     * Test scenario states:
     * Test the invocation of getCurrency method
     */
    @Test
    fun testInteractorInvocation() {

        runBlocking {

            Mockito.verify(useCases, Mockito.times(0)).getCurrency("EUR")
            useCases.getCurrency("EUR")
            Mockito.verify(useCases, Mockito.times(1)).getCurrency("EUR")

        }
    }
}