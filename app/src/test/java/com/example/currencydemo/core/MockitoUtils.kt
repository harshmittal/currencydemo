package com.example.currencydemo.core

import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing

inline fun <reified T> mock() = Mockito.mock(T::class.java)
inline fun <T> whenever(methodCall: T) : OngoingStubbing<T> = Mockito.`when`(methodCall)

inline fun <T> any(): T {
    Mockito.any<T>()
    return uninitialized()
}
inline fun <T> uninitialized(): T = null as T