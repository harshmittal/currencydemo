package com.example.currencydemo.core.data

import com.example.currencydemo.core.currencyMockData
import com.example.currencydemo.core.data.api.CurrencyAPI
import com.example.currencydemo.core.data.datasource.CurrencyAPIDatSourceImpl
import com.example.currencydemo.core.data.datasource.CurrencyAPIDataSource
import com.example.currencydemo.core.data.model.CurrencyModel
import com.example.currencydemo.core.mock
import com.example.currencydemo.core.whenever
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito
import retrofit2.Response

class CurrencyDownloaderTest {
    private lateinit var api: CurrencyAPI
    private lateinit var apiDataStore: CurrencyAPIDataSource

    @Before
    fun before() {
        api = mock<CurrencyAPI>()
        apiDataStore = CurrencyAPIDatSourceImpl(api)
    }

    /**
     * Test scenario states:
     * Test success API response
     */
    @Test(expected = Exception::class)
    fun testValidAPIResponse() {

        runBlocking{

            val response = Response.success(200,
                currencyMockData()
            )
            whenever(api.getCurrency("EUR")).thenReturn(response)
            apiDataStore.getCurrency("EUR")
            Mockito.verify(api, Mockito.times(1)).getCurrency("EUR")
        }
    }

    fun <T : Any> safeEq(value: T): T = eq(value) ?: value



    /**
     * Test scenario states:
     * Test Failure API response
     */
    @Test(expected = Exception::class)
    fun errorResponseTest() {
        runBlocking{
            val errorResponse = Response.error<CurrencyModel>(400,
                currencyMockData().toString().toResponseBody("application/json; charset=utf-8".toMediaTypeOrNull())
            )
            Mockito.`when`(api.getCurrency( "EUR")).thenReturn(errorResponse)
            apiDataStore.getCurrency("EUR")
            Mockito.verify(api, Mockito.times(1)).getCurrency("EUR")
        }
    }
}

