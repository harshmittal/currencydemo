package com.example.currencydemo.core

import com.example.currencydemo.core.data.model.CurrencyModel

fun currencyMockData() =  CurrencyModel("EUR", dummyMap() )

fun dummyMap() : LinkedHashMap<String, String>{
    val map = LinkedHashMap<String, String>()
    map["AUD"] = "1.6148"
    return map
}
