package com.example.currencydemo.core.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.currencydemo.core.data.model.CurrencyModel
import com.example.currencydemo.core.domain.CurrencyUseCases
import com.example.currencydemo.util.EspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CurrencyViewModel constructor(private val usesCases: CurrencyUseCases,
                                    private val ioDispatcher: CoroutineDispatcher,
                                    private val mainDispatcher: CoroutineDispatcher
) : ViewModel(){

    val listState = MutableLiveData<CurrencyStateLists>()

    init {

        listState.value =
            LoadingState(CurrencyModel())
    }

    /**
     * Method to get the currency list from API.
     */
    fun getCurrencyList(base: String) {
        viewModelScope.launch(ioDispatcher + exceptionHandler) {
            notifyEspressoAppIsBusy()
            val currencyList = usesCases.getCurrency(base) ?: obtainCurrentData()
            withContext(mainDispatcher) {
                listState.value =
                    LoadedState(currencyList)
                notifyEspressoAppIsIdle()
            }
        }
    }


    /**
     * Handling exception from API
     */
    private val exceptionHandler = CoroutineExceptionHandler { _, e ->
        viewModelScope.launch(mainDispatcher) {
            listState.value =
                ErrorState(e.message, obtainCurrentData())
            notifyEspressoAppIsIdle()
        }
    }

    /**
     * Espresso Busy state.
     * Use this in Espresso UI testing
     */
    private fun notifyEspressoAppIsBusy() {
        EspressoIdlingResource.increment() // App is busy until further notice
    }

    /**
     * Espresso Idle state.
     * Use this in Espresso UI testing
     */
    private fun notifyEspressoAppIsIdle(){
        // let's make sure the app is still marked as busy then decrement
        if (!EspressoIdlingResource.idlingResource.isIdleNow) {
            EspressoIdlingResource.decrement() // Set app as idle.
        }
    }

    private fun obtainCurrentData() = listState.value?.data ?: CurrencyModel()
}