package com.example.currencydemo.core.data.repository

import com.example.currencydemo.core.data.datasource.CurrencyAPIDatSourceImpl
import com.example.currencydemo.core.data.model.CurrencyModel

class CurrencyDownloader (private val remoteDataStore: CurrencyAPIDatSourceImpl): CurrencyRepository{

    override suspend fun getCurrency(base: String): CurrencyModel {
        return remoteDataStore.getCurrency(base)
    }

}