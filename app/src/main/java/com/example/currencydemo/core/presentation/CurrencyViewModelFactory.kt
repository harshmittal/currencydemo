package com.example.currencydemo.core.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.currencydemo.core.domain.CurrencyInteractor
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class CurrencyViewModelFactory @Inject constructor(private var interactor: CurrencyInteractor) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return CurrencyViewModel(interactor, Dispatchers.IO, Dispatchers.Main) as T
    }

}