package com.example.currencydemo.core.data.repository

import com.example.currencydemo.core.data.model.CurrencyModel

/**
 * Interface to define repository methods used by data source.
 */
interface CurrencyRepository{

    suspend fun getCurrency(base: String): CurrencyModel
}