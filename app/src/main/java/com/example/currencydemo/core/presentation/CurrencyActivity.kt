package com.example.currencydemo.core.presentation

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.currencydemo.R
import com.example.currencydemo.common.CurrencyApplication
import com.example.currencydemo.common.util.NetworkUtil
import com.example.currencydemo.core.di.DaggerCurrencyComponent
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class CurrencyActivity : AppCompatActivity(), CurrencyAdapter.EditTextListener {

    @Inject
    lateinit var mViewModelFactory: CurrencyViewModelFactory

    private lateinit var mViewModel: CurrencyViewModel

    lateinit var mAdapter: CurrencyAdapter

    private var mSelectedCurrency: String = "EUR"
    var mSelectedCurrencyValue: String = "1"
    private var mFocusedCurrency: String = ""
    private var scheduleTaskExecutor: ScheduledExecutorService? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerCurrencyComponent
            .builder()
            .appComponent((applicationContext as CurrencyApplication).mAppComponent).build()
            .inject(this)


        mViewModel = ViewModelProvider(this, mViewModelFactory).get(CurrencyViewModel::class.java)
        mViewModel.listState.observe(this, stateObserver)
        mAdapter = CurrencyAdapter(this, LinkedHashMap(), this)
        items_recyclerview.adapter = mAdapter
        items_recyclerview.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        getCurrencyList()
        swipeToRefresh()

    }

    private fun getCurrencyList() {
        mViewModel.getCurrencyList(mSelectedCurrency)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        scheduleTaskExecutor = Executors.newScheduledThreadPool(5)

        scheduleTaskExecutor?.scheduleAtFixedRate({
            mViewModel.getCurrencyList(mSelectedCurrency)
        }, 0, 1, TimeUnit.SECONDS)

    }

    override fun onPause() {
        super.onPause()
        scheduleTaskExecutor?.shutdown()
        scheduleTaskExecutor = null
    }

    /**
     * Pull to refresh the screen
     */
    private fun swipeToRefresh() {
        swipe_refresh.setOnRefreshListener {
            if (NetworkUtil.isNetworkConnected(this)) {
                getCurrencyList()
            } else {
                swipe_refresh.isRefreshing = false
            }
        }
    }

    /**
     *  Observer state
     */
    private val stateObserver = Observer<CurrencyStateLists> { state ->
        state?.let {
            swipe_refresh.isRefreshing = false
            when (state) {
                is LoadingState -> {
                    items_recyclerview.visibility = View.GONE
                    progress_bar.visibility = View.VISIBLE
                    no_content_textView.visibility = View.GONE
                }
                is LoadedState -> {
                    items_recyclerview.visibility = View.VISIBLE
                    progress_bar.visibility = View.GONE
                    no_content_textView.visibility = View.GONE
                    refreshAdapter(it.data.rates, true)
                }

                is ErrorState -> {
                    items_recyclerview.visibility = View.GONE
                    progress_bar.visibility = View.GONE
                    no_content_textView.visibility = View.VISIBLE
                }
            }
        }
    }

    /**
     * Method to refresh the currency
     * It multiples the currency amount entered by the user with the currency amount from API
     */
    private fun refreshAdapter(map: LinkedHashMap<String, String>, multiplay: Boolean) {
        val keys: Set<String> = map.keys
        if (multiplay) {
            if (mSelectedCurrencyValue.isNotEmpty()) {
                for (k in keys) {
                    var value: Float = map[k]?.toFloat()!!
                    value *= mSelectedCurrencyValue.toFloat()
                    map[k] = value.toString()
                }
            }
        }
        val newMap: LinkedHashMap<String, String> = map.clone() as LinkedHashMap<String, String>
        map.clear()
        map[mSelectedCurrency] = mSelectedCurrencyValue
        map.putAll(newMap)
        mAdapter.refreshAdapter(map)
    }

    override fun onDestroy() {
        mViewModel.listState.removeObserver(stateObserver)
        super.onDestroy()
    }

    override fun onFocusChanged(currency: String, currencyValue: String, position: Int) {
        mSelectedCurrency = currency
        mSelectedCurrencyValue = currencyValue
        if (mSelectedCurrency != mFocusedCurrency) {
            mFocusedCurrency = mSelectedCurrency
            refreshAdapter(mAdapter.mCurrency, false)
        }
    }

    /**
     * Call back method from Adapter when user starts modifying the currency
     */
    override fun updateCurrency(currencyValue: String) {
        mSelectedCurrencyValue = currencyValue
        Handler().postDelayed(Runnable {
            if (mSelectedCurrencyValue.isNotEmpty()) {
                getCurrencyList()
            }
        }, 2000)
    }
}
