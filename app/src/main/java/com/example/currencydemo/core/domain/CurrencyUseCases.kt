package com.example.currencydemo.core.domain

import com.example.currencydemo.core.data.model.CurrencyModel

/**
 * Interface to define business logic
 */
interface CurrencyUseCases{

    /**
     * Get currency use case
     */
    suspend fun getCurrency(base: String): CurrencyModel
}