package com.example.currencydemo.core.presentation

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.currencydemo.R
import kotlinx.android.synthetic.main.item_currency.view.*
import java.util.*
import kotlin.collections.ArrayList


class CurrencyAdapter(
    var mContext: Context,
    public var mCurrency: LinkedHashMap<String, String>,
    private var mListener: EditTextListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_TITLE = 0x1f
        const val TYPE_CURRENCY_ROW = 0x2f
    }

    public fun refreshAdapter(map: LinkedHashMap<String, String>) {
        mCurrency = map
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: RecyclerView.ViewHolder
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            TYPE_TITLE -> {
                view = inflater.inflate(R.layout.item_title, parent, false)
                viewHolder = TextViewHolder(view)
            }
            TYPE_CURRENCY_ROW -> {
                view = inflater.inflate(R.layout.item_currency, parent, false)
                viewHolder = CurrencyViewHolder(view, mListener)
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position != 0) {
            val keys = ArrayList<String>(mCurrency.keys)
            val currency = keys[position - 1]
            val currencyValue = mCurrency[currency]
            (holder as CurrencyViewHolder).setData(currency, currencyValue!!, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            TYPE_TITLE
        } else {
            TYPE_CURRENCY_ROW
        }
    }

    override fun getItemCount(): Int {
        return mCurrency.size + 1
    }

    class TextViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    class CurrencyViewHolder(var view: View, var listener: EditTextListener?) :
        RecyclerView.ViewHolder(view), TextWatcher {

        var currency = ""
        var currencyValue = ""
        var pos = 0

        init {

            view.currency_value_editText.onFocusChangeListener =
                View.OnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) {
                        listener?.onFocusChanged(currency, currencyValue, pos)
                        view.currency_value_editText.addTextChangedListener(this)
                        if (!view.currency_value_editText.text.isNullOrEmpty()) {
                            view.currency_value_editText.setSelection(view.currency_value_editText.text.length)
                        }
                    }
                }
        }

        public fun setData(currency: String, currencyValue: String, position: Int) {
            pos = position
            this.currencyValue = currencyValue
            this.currency = currency
            view.currency_code_textView.text = currency
            view.currency_name_textView.text = Currency.getInstance(currency).displayName
            view.currency_value_editText.setText(currencyValue, TextView.BufferType.EDITABLE)

            Glide.with(view.context)
                .load(R.drawable.dummy_currency)
                .apply(RequestOptions.circleCropTransform())
                .into(view.img_profile)
        }

        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            if ((start > 0 && before > 0) || (before == 0 && count > 0) || before == 1 && count == 0) {
                listener?.updateCurrency(s.toString())
            }
        }
    }

    interface EditTextListener {
        fun onFocusChanged(currency: String, currencyValue: String, position: Int)
        fun updateCurrency(currencyValue: String)
    }

}