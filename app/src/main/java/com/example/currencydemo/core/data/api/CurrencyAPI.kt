package com.example.currencydemo.core.data.api

import com.example.currencydemo.core.data.model.CurrencyModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyAPI{

    @GET("latest")
    suspend fun getCurrency(@Query("base") base: String): Response<CurrencyModel>?
}