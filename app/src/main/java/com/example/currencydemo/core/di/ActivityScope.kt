package com.example.currencydemo.core.di

import javax.inject.Scope

@Scope
annotation class ActivityScope