package com.example.currencydemo.core.data.datasource

import com.example.currencydemo.core.data.model.CurrencyModel

/**
 * API data source.
 */
interface CurrencyAPIDataSource{

    suspend fun getCurrency(base: String): CurrencyModel
}