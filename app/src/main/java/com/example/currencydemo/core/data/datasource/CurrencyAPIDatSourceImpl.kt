package com.example.currencydemo.core.data.datasource

import com.example.currencydemo.core.data.api.CurrencyAPI
import com.example.currencydemo.core.data.model.CurrencyModel
import retrofit2.Response
import javax.inject.Inject

class CurrencyAPIDatSourceImpl @Inject constructor(private var api: CurrencyAPI) : CurrencyAPIDataSource {

    override suspend fun getCurrency(base: String): CurrencyModel {
        val response: Response<CurrencyModel>? = api.getCurrency(base)
        if (response != null && response.code() == 200) {
            return (response.body() as CurrencyModel)
        } else {
            throw Exception()
        }
    }

}