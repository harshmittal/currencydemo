package com.example.currencydemo.core.presentation

import com.example.currencydemo.core.data.model.CurrencyModel

sealed class CurrencyStateLists{
    abstract var data: CurrencyModel
}

data class LoadingState(override var data: CurrencyModel) : CurrencyStateLists()
data class LoadedState(override var data: CurrencyModel) : CurrencyStateLists()
data class ErrorState(val errorMessage: String?, override var data: CurrencyModel) : CurrencyStateLists()