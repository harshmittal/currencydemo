package com.example.currencydemo.core.di

import com.example.currencydemo.core.presentation.CurrencyActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [CurrencyModule::class])
interface CurrencyComponent{

    fun inject(activity: CurrencyActivity)
}