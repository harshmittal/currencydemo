package com.example.currencydemo.core.di

import android.app.Application
import com.example.currencydemo.common.CurrencyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideApp(application: Application) : CurrencyApplication = application as CurrencyApplication
}