package com.example.currencydemo.core.di

import com.example.currencydemo.core.data.api.CurrencyAPI
import com.example.currencydemo.core.data.datasource.CurrencyAPIDatSourceImpl
import com.example.currencydemo.core.data.repository.CurrencyDownloader
import com.example.currencydemo.core.data.repository.CurrencyRepository
import com.example.currencydemo.core.domain.CurrencyInteractor
import com.example.currencydemo.core.domain.CurrencyUseCases
import com.example.currencydemo.core.presentation.CurrencyViewModelFactory
import com.example.currencydemo.network.APIUtil
import dagger.Module
import dagger.Provides

@Module
class CurrencyModule {

    @Provides
    @ActivityScope
    fun provideViewModelFactory(interactor: CurrencyInteractor): CurrencyViewModelFactory {
        return CurrencyViewModelFactory(interactor)
    }

    @Provides
    @ActivityScope
    fun providesInteractor(repository: CurrencyRepository): CurrencyUseCases {
        return CurrencyInteractor(repository)
    }

    @Provides
    @ActivityScope
    fun providesRepository( apiDataSource :CurrencyAPIDatSourceImpl): CurrencyRepository {
        return CurrencyDownloader(apiDataSource)
    }

    @Provides
    @ActivityScope
    fun providesAPISource(api: CurrencyAPI): CurrencyAPIDatSourceImpl {
        return CurrencyAPIDatSourceImpl(api)
    }

    @Provides
    @ActivityScope
    fun providesAPI(): CurrencyAPI {
        return APIUtil.getRetrofit().create(CurrencyAPI::class.java)
    }
}