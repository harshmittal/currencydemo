package com.example.currencydemo.core.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurrencyModel(var base: String, var rates: LinkedHashMap<String, String>):
    Parcelable {
    constructor() : this("", LinkedHashMap())

}