package com.example.currencydemo.core.domain

import com.example.currencydemo.core.data.model.CurrencyModel
import com.example.currencydemo.core.data.repository.CurrencyRepository
import javax.inject.Inject

/**
 * Implementation of business logic
 */
class CurrencyInteractor @Inject constructor(private val repository: CurrencyRepository) : CurrencyUseCases{

    override suspend fun getCurrency(base: String): CurrencyModel {
        return repository.getCurrency(base)
    }

}