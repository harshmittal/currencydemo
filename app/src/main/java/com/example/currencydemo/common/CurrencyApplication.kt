package com.example.currencydemo.common

import android.content.Context
import com.example.currencydemo.core.di.AppComponent
import com.example.currencydemo.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class CurrencyApplication : DaggerApplication(){

    lateinit var mAppComponent: AppComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = mAppComponent

    override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        appContext = this
        initDaggerComponent()
    }

    open fun initDaggerComponent() {
        mAppComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    companion object {
        @get:Synchronized
        var appContext: Context? = null
            private set
    }
}