package com.example.currencydemo.common.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkInfo

/**
 * Util class for Network
 */
object NetworkUtil {

    /**
     * Class to check Network
     * @return boolean true if devices is connected to network else false
     * @param context activity context
     */
    fun isNetworkConnected(context: Context): Boolean {
        var isConnected: Boolean = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            val network: Network? = connectivityManager?.activeNetwork
            if (network != null) {
                val capabilities = connectivityManager.getNetworkCapabilities(network)
                isConnected = capabilities != null && (capabilities.hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
                ) || capabilities.hasTransport(
                    NetworkCapabilities.TRANSPORT_CELLULAR
                ))
            }
        } else {
            val activeNetwork: NetworkInfo? = connectivityManager?.activeNetworkInfo
            if (activeNetwork != null) {

                isConnected =
                    (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
            }
        }
        return isConnected
    }
}