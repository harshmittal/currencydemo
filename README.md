# Project Title

Currency Demo

## Getting Started

Import the project in an IDE

### Prerequisites

Android Studio 3.5.3 with API level 29 API tools installed
Gradle version 3.5.3
Minimum SDK Version supported is API 21
Target SDK version is API 29

### Installing

Install APK by running the app from studio or generating the apk by building the app using command

```
.gradlew assembleDebug
```

## Running the tests

Run unit tests using command
```
./gradlew test
```

Run instrument tests using command
```
./gradlew connectedAndroidTest
```

### Break down into end to end tests

Data layer and View model are tested with Unit test cases using Mockito while Espresso is used for UI testing.


### Coding style

Kotlin is used as a programming language.
Application is written in clean architecture with MVVM design pattern.
Dagger 2 is used as a Dependency Injection Framework.
It can be divided into 3 parts 

1. Data layer : 
This layer is responsible to make API call and have data model. 
CurrencyAPIDatSourceImpl class is reponsible for making API call and getting the result back.
CurrencyRepository this class is the connector between data layer and domain layer
 
2. Domain layer :
This layer defines business logic to be implemented.
CurrencyUseCases this class is a connector between View Model and domain layer

3. Presentation layer :
This class consists of UI in form of activity.
It also have View Model which implements reactive programming.

## Improvements

Currency images are replaced with dummy circular placeholder. We can map currency codes with images either via API or storing them locally. 

## Versioning

1.0 

